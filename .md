__NOTE:__ This is simplified. However, it's a useful high-level model for determining what kind of database you need for your project.

## Data models

* __Documents:__ Single type of thing, no relations
* __Relational:__ Multiple types of things, relations between different types of things
* __Graph:__ Single or multiple types of things, relations between different things of the *same* type

## Consistency models

* __Strong consistency:__ There is a single canonical view of the database, and everything connecting to any node in the database cluster is guaranteed to see the same data at the same moment in time.
* __Eventual consistency:__ There can be multiple different views of the database (eg. different nodes in the cluster may have a different idea of what the current state of the data is), but once you stop changing stuff, they will eventually converge into a single view.
* __No consistency:__ There's no guarantee that all nodes in the cluster will ever converge to the same view, whatsoever.

## Schemafulness

* __Schemaful:__ You know the format (fields, types, etc.) of the data upfront. Fields may be optional, but every field you use is defined in the schema upfront.
* __Schemaless:__ You have no idea what the format is going to be. This is rarely applicable, and only really applies when dealing with storing data from a source that doesn't produce data in a reliable format.